package main

import (
	"fmt"

	"gitlab.com/arunnasarain008/stack-implementation/stack"
)

func main() {

	stack := stack.Stack{}

	stack.Push(100)
	element, notEmpty := stack.Pop()

	if !notEmpty {
		fmt.Println(notEmpty)
	} else {
		fmt.Println(element)
	}

}
