package stack

import (
	"testing"
)

func TestPop(t *testing.T) {
	stack := Stack{}

	var testCases = []struct {
		element int
	}{
		{1},
		{2},
		{3},
		{4},
		{5},
		{6},
	}

	for _, x := range testCases {

		_ = stack.Push(x.element)

		result, _ := stack.Pop()

		if result == x.element {
			t.Log("Success")
		} else {
			t.Error("Expecting 100")
		}
	}

}

func TestPush(t *testing.T) {

	stack := Stack{}

	var testCases = []struct {
		element int
	}{
		{1},
		{2},
		{3},
		{4},
		{5},
		{6},
		{5},
	}

	for _, x := range testCases {

		err := stack.Push(x.element)
		if err != nil {
			t.Error(err.Error())
		} else {
			t.Log("Success")
		}
	}
}
