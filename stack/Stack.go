package stack

import (
	"errors"
	"reflect"
)

//Stack stores elements of type int
type Stack []int

//Pop returns the top element of the stack and deletes it from stack
func (stack *Stack) Pop() (int, bool) {

	len := len(*stack)

	if len == 0 {
		return 0, false
	}
	top := len - 1
	element := (*stack)[top]
	*stack = (*stack)[:top]
	return element, true
}

//Push appends the new element to stack
func (stack *Stack) Push(new int) error {
	if reflect.TypeOf(new) != reflect.TypeOf(0) {
		return errors.New("Invalid type")
	} else if len(*stack) > 10 {
		return errors.New("Stack limit reached")
	}
	*stack = append(*stack, new)
	return nil
}
